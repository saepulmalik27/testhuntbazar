step by step running this project

1. run command composer install
2. run command npm install
3. npm run dev
4. edit .env.example to .env and setup your database connection on .env file
5. run command php artisan key:generate
6. run command php artisan migrate
7. php artisan db:seed
8. php artisan serve --> running at port 8000
9. user : admin password : admin123
