<?php

namespace App\Http\Controllers;

use App\Invitation;
use App\Jobs\SendEmail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class InvitationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $result = Invitation::leftJoin("invitation_users as iu", function ($join) {
            $join->on('iu.email', '=', 'invitations.email');
        })->select('invitations.email', 'iu.name', 'iu.birth_date', 'iu.gender')->get();
        return response($result->jsonSerialize(), Response::HTTP_OK);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $form = $request->all();
        try {
            // dd($form['email']);
            $result = Invitation::create($form);
            $base_url = env("IMAP_HOSTNAME_TEST", "localhost:8000");
            $link = "$base_url/invite/$result->id";
            $name = explode('@', $form['email']);
            $details = ['email' => $form['email'], 'link' => $link, 'name' => $name[0]];
            $emailJob = (new SendEmail($details))->delay(Carbon::now()->addHour(1));
            dispatch($emailJob);

            return response(['data' => $result, 'message' => 'succesfuly created'], Response::HTTP_CREATED);

        } catch (\Throwable $th) {
            return $th;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Invitation  $invitation
     * @return \Illuminate\Http\Response
     */
    public function show(Invitation $invitation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Invitation  $invitation
     * @return \Illuminate\Http\Response
     */
    public function edit(Invitation $invitation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Invitation  $invitation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Invitation $invitation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Invitation  $invitation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Invitation $invitation)
    {
        //
    }
}
