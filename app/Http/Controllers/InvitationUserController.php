<?php

namespace App\Http\Controllers;

use App\Invitation;
use App\InvitationUser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class InvitationUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = InvitationUser::select('*')->get();
        return response($result->jsonSerialize(), Response::HTTP_OK);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // dd("berhasil");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $form = $request->all();

        try {
            $result = InvitationUser::create($form);
            $result->Designers()->sync($form['designers']);
            return response(['data' => $result, 'message' => 'data succesfully created'], Response::HTTP_CREATED);
        } catch (\Throwable $th) {
            return $th;
        }
    }

    public function invite($id)
    {
        $record = Invitation::findOrFail($id);

        if (!empty($record)) {
            // dd($record->email);
            $result = InvitationUser::where('email', $record->email)->first();
            if (empty($result)) {
                // dd($result);
                return view('invitation.add', compact('record'));
            } else {
                // dd($result);
                return view('invitation.card', compact('record', 'result'));
            }

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InvitationUser  $invitationUser
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = InvitationUser::with('Designers')->findOrFail($id);
        return response($result, Response::HTTP_OK);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InvitationUser  $invitationUser
     * @return \Illuminate\Http\Response
     */
    public function edit(InvitationUser $invitationUser)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InvitationUser  $invitationUser
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InvitationUser $invitationUser)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InvitationUser  $invitationUser
     * @return \Illuminate\Http\Response
     */
    public function destroy(InvitationUser $invitationUser)
    {
        //
    }
}
