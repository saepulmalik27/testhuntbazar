<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvitationUser extends Model
{

    protected $fillable = [
        'email', 'birth_date', 'gender', 'name', 'code',
    ];

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($builder) {
            $conn = $builder->getModel()->getConnection();
            $table = $builder->getModel()->getTable();
            $year = date("y");
            $month = date("m");
            $builder->code = "INV/$year-$month/$builder->email";

        });

    }

    public function Designers()
    {
        return $this->belongsToMany(Designer::class, 'invitation_users_designers', 'invitation_user_id', 'designer_id');
    }
}
