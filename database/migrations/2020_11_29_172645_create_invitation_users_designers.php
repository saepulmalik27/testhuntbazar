<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvitationUsersDesigners extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invitation_users_designers', function (Blueprint $table) {
            $table->integer('invitation_user_id')->unsigned();
            $table->foreign('invitation_user_id')->references('id')->on('invitation_users');
            $table->integer('designer_id')->unsigned();
            $table->foreign('designer_id')->references('id')->on('designers');
            $table->primary(['invitation_user_id', 'designer_id']);
            // $table->primary(['parent_id', 'child_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invitation_users_designers');
    }
}
