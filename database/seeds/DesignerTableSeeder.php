<?php

use Illuminate\Database\Seeder;

class DesignerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $designer = [
            'Balenciaga', 'Berluti', 'Aldies',
            'Each x Other',
        ];
        foreach ($designer as $key => $value) {
            DB::table('designers')->insert([
                'name' => $value,
            ]);
        }

    }
}
