/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import  './bootstrap';
import Vue from 'vue';

import VueMultiselect from "vue-multiselect";
Vue.component("multiselect", VueMultiselect);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

import App from './components/ExampleComponent'
import Countdown from "./components/Countdown"; 
import Headcomponent from "./components/Header";
import FormUndangan from "./views/FormUndangan"
import ListUndangan from "./views/ListUndangan"

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
if (document.getElementById("app")) {
const app = new Vue({
    el: '#app',
    render : h => h(App)
});
}

if(document.getElementById("countdown")){
const countdown = new Vue({
    el: '#countdown',
    // render : h => h(Countdown)
    components: {
        Countdown
    },
});
}

if(document.getElementById("header")){
    const header = new Vue({
        el: '#header',
        // render : h => h(Countdown)
        components: {
            Headcomponent
        },
    });
}


if(document.getElementById("main")){
    const header = new Vue({
        el: '#main',
        // render : h => h(Countdown)
        components: {
            FormUndangan
        },
    });
}

if(document.getElementById("list-undangan")){
    const header = new Vue({
        el: '#list-undangan',
        // render : h => h(Countdown)
        components: {
            ListUndangan
        },
    });
}
