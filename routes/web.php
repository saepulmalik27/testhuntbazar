<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('welcome');
});

Route::get('invite/{id}', 'invitationUserController@invite')->name('invite.user');
Route::resource('invitation-users', 'InvitationUserController');
Route::resource('invitations', 'InvitationController');

// Route::post('invitation-users-post', 'InvitationUserController@store')->name('store.invitation');
Route::resource('designers', 'DesignerController');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
